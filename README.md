## Marvel App

An application used to display characters from Marvel's Universe, built with React, Redux, JavaScript, and CSS.

## Project Status

In Progress.

## Installation and Setup Instructions

Clone down this repository. You will need `npm` installed globally on your machine.

### Installation:

`npm install`

To Start the Server:

`npm start`

To Visit the App:

`localhost:8080/`
