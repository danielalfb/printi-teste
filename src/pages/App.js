import React, { useState, useEffect } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { createGlobalStyle } from 'styled-components';
import ViewCharactersPage from './ViewCharactersPage';
import Characters from './Characters';

const GlobalStyle = createGlobalStyle`

  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  
  body {
    font-family: 'Poppins', sans-serif;
    background-color: #f4f4f4;
  }
`;

export default function App() {
  return (
    <>
      <GlobalStyle />
      <BrowserRouter>
        <div>
          <Switch>
            <Route exact path="/" component={Characters} />
            <Route exact path="/character/:id" component={ViewCharactersPage} />
          </Switch>
        </div>
      </BrowserRouter>
    </>
  );
}
