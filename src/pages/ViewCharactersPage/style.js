import styled from 'styled-components';

export const NavBar = styled.div`
  width: 100%;
  display: flex;
  background-color: #f78f3f;
  color: #fff;
  padding: 15px;
  border: none;
  font-size: 18px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  text-decoration: none;

  button {
    width: 65px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    background: none;
    border: none;
    font-size: 18px;
    font-family: 'Poppins', sans-serif;
    color: #fff;
    font-weight: 500;
    cursor: pointer;
  }

  h1 {
    font-size: 22px;
  }
`;
export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  margin-top: 50px;

  @media screen and (max-width: 600px) {
    padding: 0 30px;
  }
`;

export const FirstContainer = styled.div`
  width: 100%;
  max-width: 1200px;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
  background-color: #fff;
  border-radius: 8px;
  padding: 20px;

  img {
    width: 400px;
    height: auto;
    margin-right: 20px;
  }

  .description-content {
    width: 100%;
  }

  @media screen and (max-width: 600px) {
    flex-direction: column;
    align-items: center;
    justify-content: center;

    img {
      margin-right: 0;
    }
  }
`;

export const SecondContainer = styled.div`
  width: 100%;
  max-width: 1200px;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 30px;
  background-color: #fff;
  border-radius: 8px;
  padding: 20px;

  a {
    width: 200px;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: #f78f3f;
    font-size: 18px;
    font-family: 'Poppins', sans-serif;
    color: #fff;
    text-decoration: none;
    font-weight: 500;
    cursor: pointer;
    padding: 10px;
    border-radius: 8px;
  }

  .stories-list {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: center;
    margin: 40px 0;
  }

  .story-item {
    display: flex;
    align-items: center;
    width: 250px;
    height: 150px;
    padding: 20px;
    margin: 10px;
    background-color: #fff;
    border-radius: 8px;
    box-shadow: 0 0 10px 0px #d0d0d0;
  }
`;
