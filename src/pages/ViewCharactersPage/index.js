import React from 'react';
import { useLocation, Link } from 'react-router-dom';
import { IoMdArrowRoundBack } from 'react-icons/io';
import { NavBar, FirstContainer, SecondContainer, Container } from './style';

const ViewCharactersPage = (_) => {
  const { state } = useLocation();
  const stories = [...state.comics.items];

  return (
    <>
      <NavBar>
        <Link to="/">
          <button>
            <IoMdArrowRoundBack /> <span>Back</span>
          </button>
        </Link>
        <h1>{state.name}</h1>
      </NavBar>
      <Container>
        <FirstContainer>
          <img
            src={`${state.thumbnail.path}.${state.thumbnail.extension}`}
            alt={`${state.name}'s image`}
          />
          <div className="description-content">
            <h1>{state.name}</h1>
            <p>
              {(() => {
                if (state.description) {
                  return state.description;
                }
                return 'There is no description available';
              })()}
            </p>
          </div>
        </FirstContainer>
        <SecondContainer>
          <h1>Stories</h1>
          <div className="stories-list">
            {stories &&
              stories.map((story) => {
                return <div className="story-item">{story.name}</div>;
              })}
          </div>
          <a
            href={state.urls[0].url}
            alt="Click to view more at Marvel's website"
            target="_blank"
          >
            View more
          </a>
        </SecondContainer>
      </Container>
    </>
  );
};

export default ViewCharactersPage;
