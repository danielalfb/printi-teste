import React, { useEffect, useState, useMemo } from 'react';
import CharactersPost from '../../components/CharactersPost';
import Pagination from '../../components/Pagination';
import api from '../../services/api';

function Characters() {
  const [characters, setCharacters] = useState([]);

  useEffect(() => {
    api
      .get('/characters?limit=100')
      .then(async (res) => await res.data.data.results)
      .then((result) => setCharacters(result));
  }, []);

  return (
    <div>
      {characters.length > 0 ? (
        <>
          <Pagination
            data={characters}
            RenderComponent={CharactersPost}
            title="Characters"
            pageLimit={5}
            dataLimit={10}
          />
        </>
      ) : (
        <h1>Loading...</h1>
      )}
    </div>
  );
}

export default Characters;
