import styled from 'styled-components';

const Box = styled.div`
  .post {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    width: 420px;
    height: 150px;
    margin: 20px;
    padding: 15px;
    background: #fff;
    box-shadow: 0px 0px 10px 0px #d0d0d0;
    border-radius: 8px;

    @media screen and (max-width: 600px) {
      width: 100%;
    }
  }
  img {
    width: 110px;
    height: auto;
    border-radius: 8px;
    margin-right: 10px;
  }
  h1 {
    width: 200px;
    font-size: 20px;
    font-weight: 400;
    color: #181616;
  }

  a {
    color: #504a4a;
    margin: 0 10px 20px 0;
    font-size: 18px;

    &:hover {
      color: #f78f3f;
    }
  }
`;

export default Box;
