import React from 'react';
import Box from './style';
import { FaPlus } from 'react-icons/fa';
import { Link } from 'react-router-dom';

const CharactersPost = (props) => {
  const { id, name, thumbnail, description, comics, urls } = props.data;
  return (
    <>
      <Box>
        {' '}
        <div className="post">
          <img src={`${thumbnail.path}.${thumbnail.extension}`} />
          <h1>{name}</h1>
          <div>
            <Link
              to={{
                pathname: `/character/${id}`,
                state: { id, name, thumbnail, description, comics, urls },
              }}
              alt="Details"
            >
              <FaPlus />
            </Link>
          </div>
        </div>
      </Box>
    </>
  );
};

export default CharactersPost;
