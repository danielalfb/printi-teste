import styled from 'styled-components';

const Box = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: 30px 0;

  .title {
    text-align: center;
    color: #e23636;
  }

  .pagination {
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 20px;
  }

  .paginationItem {
    padding: 10px 15px;
    height: 45px;
    width: 45px;
    position: relative;
    margin: 0 5px;
    cursor: pointer;
    border-radius: 50%;
    border: none;
  }

  .paginationItem span {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  .prev,
  .next {
    background: none;
    border: none;
    padding: 10px;
    color: #e23636;
    margin: 0 10px;
    cursor: pointer;
  }

  .paginationItem.active {
    font-weight: 800;
    background-color: #e23636;
    color: #fff;
    pointer-events: none;
  }

  .prev.disabled,
  .next.disabled {
    pointer-events: none;
    box-shadow: none;
    color: #999;
  }

  .dataContainer {
    width: 100%;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    margin: 20px 0;

    @media screen and (max-width: 600px) {
      width: 100%;
    }
  }
`;

export default Box;
