import axios from 'axios';
import md5 from 'md5';

const publicKey = '44e1c1537a8eda27cafb8e1649474289';
const privateKey = '5d47db8c4442678b08aabc734397e10ab7bba1bd';

const ts = Number(new Date());

const hash = md5(ts + privateKey + publicKey);

const api = axios.create({
  baseURL: 'https://gateway.marvel.com/v1/public/',
  params: {
    ts,
    apikey: publicKey,
    hash,
  },
});

export default api;
